package services 

// TODO: I need to pass in the cmd_hndlr client somehow to this service.
// Previously the clients were passed in to the controllers. But I believe that to be a mistake. I believe
// that the food chain should be that the services are passed into the controllers, and that the clients are passed into the services. Also
// the repository should be passed into the service.
// This will allow me to test the controllers separate from the underlying service as well as the repo, which isolates the logic decision tree.
// This also allows me to test the service in isolation from the underlying client that it depends on.

import (
	"fmt"
	"log"
	//"os"
	"time"
	cmd_client "bitbucket.org/cornjacket/cmd_hndlr/client_lib"
	"bitbucket.org/cornjacket/iot/message"

)

type LoraPacketGenService struct {
}

var cmdHndlr cmd_client.PacketCmdHndlrService

func (s *LoraPacketGenService) Start() {

	init_service()

	go func() {
		msgCount := 0
		for {
			msgCount++
			//body := fmt.Sprintf("Hello RabbitMQ message %v", msgCount)

        		p := message.UpPacket{
                		Dr:   "?",
                		Ts:   uint64(msgCount), // TODO: need to convert current time into uint64 epoch time
                		Eui:  "1234",
                		Ack:  false,
                		Cmd:  "rx",
                		Snr:  12.4,
                		Data: "12345678", // this is a random packet that should get rejected by the controller
                		Fcnt: 12,
                		Freq: uint64(20),
                		Port: 1,
                		Rssi: -110,
        		}
			if cmdHndlr.SendPacket(p) != nil {
				fmt.Printf("LoRaPacketGenService: SendPacket failed. message_counte: %d.\n", msgCount)
			} else { 
				fmt.Printf("LoRaPacketGenService: SendPacket succeeded. message_counte: %d.\n", msgCount)
			}
			//log.Printf(" [x] Sent %s", body)
			//failOnError(err, "Failed to publish a message")

			time.Sleep(10 * time.Second)
		}
	}()

}

// Any essential initialization should happen here before the start of the go routine and before control is
// given back to the calling program. It would be dangerous to have initialization happen inside the go routine
// as the calling program might invoke the service before it was ready.
func init_service() {
	fmt.Println("LoRa Pkt Generator Init...")
	cmdHndlr.Open()	
	time.Sleep(7 * time.Second)
}


func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
