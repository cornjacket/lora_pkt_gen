// Package classification Cmd API.
//
// Documentation for Cmd API
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: http
//     Host: localhost
//     BasePath: /
//     Version: 0.1.X
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: David Taylor<taylor.david.ray@gmail.com> http://john.doe.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
// swagger:meta
package controllers

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/cornjacket/lora_pkt_gen/app/services"
	"github.com/julienschmidt/httprouter"
	//"github.com/rs/cors"
)

type LoraPacketGenServiceInterface interface {
	Start()
}

// TODO: This should be ControllersContext struct - which holds the dependencies of the controllers. Does Router really need to be stored here? Can't it
// live in controllers as a package level variable. Could all of these live inside the controllets package instead of being part of the type?
type AppContext struct {
	Router                   *httprouter.Router
	LoraPacketGenService     LoraPacketGenServiceInterface
}

// TODO: This should be NewControllersContext()
func NewAppContext() *AppContext {
	context := AppContext{}
	context.LoraPacketGenService = &services.LoraPacketGenService{} // implements LoraPacketGenServiceInterface
	return &context
}

func (app *AppContext) Run(addr string) {

	app.InitServices() // Is this where this belongs or should it belong in app?

	// Instantiate a new router
	app.Router = httprouter.New()

	app.initializeRoutes()

	// Add CORS support (Cross Origin Resource Sharing)
	//handler := cors.Default().Handler(app.Router)
	//err = http.ListenAndServe(":"+addr, handler)
	//if err != nil {
	//	logger.Fatal(err)
	//}

	//go component.Run(addr) // quick testing for now
	fmt.Printf("Listening to port %s\n", addr)
	log.Fatal(http.ListenAndServe(":"+addr, app.Router))
}

func (app *AppContext) InitServices() {
	app.LoraPacketGenService.Start()
}
