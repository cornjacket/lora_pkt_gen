package controllers

import (
	"net/http"

	"bitbucket.org/cornjacket/lora_pkt_gen/app/responses"
)

func (app *AppContext) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Service Up")

}
