// TODO: This file is borrowed from packet manager. Right now the code is not useful. It needs to be modified. However right now I
// don't have a /node interface in the lora_pkt_gen to be tested. So for now there is nothing to do with this file.

// I think I need to have a test function that is enabled via the environment in order for the service to begin generating rx packets.
// Later an interface can be made so that the user can request that packets begin to upstream for a specific EUI.


package app

import (
	"fmt"
	//"log"
	"testing"
	//"time"

	. "gopkg.in/check.v1"

	"bitbucket.org/cornjacket/lora_pkt_gen/app/controllers"
	//"bitbucket.org/cornjacket/lora_pkt_gen/app/mocks"
	//pkt_gen_client "bitbucket.org/cornjacket/lora_pkt_gen/client_lib"
	//"bitbucket.org/cornjacket/iot/message"
)

//var pktClient cmd_client.PacketCmdHndlrService

func NewAppServiceMock() AppService {
	a := AppService{}
	a.Context = NewAppContextMock()
	return a
}

func NewAppContextMock() *controllers.AppContext {
	context := controllers.AppContext{}
	//context.PacketCmdHndlrService = &mocks.PacketCmdHndlrServiceMock{}     // implements PacketCmdHandlrPostInterface
	return &context
}

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type TestSuite struct {
	AppService AppService
}

var _ = Suite(&TestSuite{})

func (s *TestSuite) SetUpSuite(c *C) {
	fmt.Println("SetUpSuite() invoked")
/*
	s.AppService = NewAppServiceMock()
	s.AppService.InitEnv()
	go s.AppService.Run()
	// time delay to guarantee so that the server is up and talking to the database
	time.Sleep(1 * time.Second)
	// Initialize Packet Gen Client
	//if err := pktClient.Open(); err != nil {
	//	log.Fatal("pktClient.Open() error: ", err)
	//}
*/

}

func (s *TestSuite) SetUpTest(c *C) {
	fmt.Println("SetupTest() invoked")
}

func (s *TestSuite) TearDownTest(c *C) {
	fmt.Println("TearDownTest() invoked")
}

func (s *TestSuite) TearDownSuite(c *C) {
	fmt.Println("TearDownSuite() invoked")
	// TODO(drt) - close the db connection
}

func (s *TestSuite) TestRxPacketPath(c *C) {
	// Send Packet
	fmt.Printf("component_test.packetTest\n")

/*
	p := message.UpPacket{
		Dr:   "?",
		Ts:   uint64(12345),
		Eui:  "1234",
		Ack:  false,
		Cmd:  "rx",
		Snr:  12.4,
		Data: "12345678", // this is a random packet that should get rejected by the controller
		Fcnt: 12,
		Freq: uint64(20),
		Port: 1,
		Rssi: -110,
	}

	err := pktClient.SendPacket(p)
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing

	c.Assert(mocks.EventLastRxPacket.Data, Equals, p.Data)
	c.Assert(mocks.EventLastRxPacket.Eui, Equals, p.Eui)
	c.Assert(mocks.EventLastRxPacket.Cmd, Equals, p.Cmd)
*/

}

