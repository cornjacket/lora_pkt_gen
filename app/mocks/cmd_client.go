package mocks

import (
	"log"

	"bitbucket.org/cornjacket/iot/message"
)

// Should I rename this clientMock
type CmdHndlrServiceMock struct {
}

var CmdLastRxPacket message.UpPacket

func (s *CmdHndlrServiceMock) Open() error {
	return nil
}

func (s *CmdHndlrServiceMock) SendPacket(packet message.UpPacket) error {
	log.Println("CmdClientMock.SendPacket() invoked.")
	CmdLastRxPacket = packet
	return nil
}
