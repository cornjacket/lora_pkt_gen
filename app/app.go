package app

import (
	//"log"
	//"strconv"

	"bitbucket.org/cornjacket/lora_pkt_gen/app/controllers"
	"bitbucket.org/cornjacket/lora_pkt_gen/app/utils/env"
	//"bitbucket.org/cornjacket/discovery"
)

type AppService struct {
	Env     AppEnv
	Context *controllers.AppContext
}

type AppEnv struct {
	PortNum string
}

func NewAppService() AppService {
	a := AppService{}
	a.Context = controllers.NewAppContext()
	return a
}

func (a *AppService) InitEnv() {
	a.Env.PortNum = env.GetVar("CMD_PORT_NUM", "8079", false)
}

func (a *AppService) SetupExternalServices() {
}

func (a *AppService) Init() {
	a.InitEnv()
	a.SetupExternalServices()
}

func (a *AppService) Run() {

	a.Context.Run(a.Env.PortNum)

}
