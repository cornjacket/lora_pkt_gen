package client_lib

import (

	"fmt"
	"encoding/json"
	"bytes"
	"net/http"
	"log"
	"time"
	"bitbucket.org/cornjacket/iot/message"
)

type PacketCmdHndlrService struct {
	Hostname  string
	Port      string
	Path      string
	Enabled   bool
}

// TODO: Validate Hostname and Port within reason
func (s *PacketCmdHndlrService) Open(Hostname, Port, Path string) error {
	s.Hostname = Hostname
	s.Port = Port
	s.Path = Path
	s.Enabled = true
	return s.Ping()	
}

func (s *PacketCmdHndlrService) URL() string {
	if s.Enabled {
		return "http://" + s.Hostname + ":" + s.Port + s.Path 
	}
	return ""
}

// Used to determine if the service is currently up.
// Ping makes multiple attempts to see if the external service is up before giving up and returning an error.
func (s *PacketCmdHndlrService) Ping() error {
        // check if enabled
        // if so, then make a GET call to root of (). We could change this to /ping
        // if there is an error, then repeatedly call for X times with linear backoff until success
        // ping loop
        var pingErr error
        for i := 0; i < 15; i++ {
                host := "http://" + s.Hostname + ":" + s.Port + "/"
                fmt.Printf("Pinging %s\n", host)
                _, pingErr = http.Get(host)
                if pingErr != nil {
                        fmt.Println(pingErr)
                } else {
                        fmt.Println("Ping replied.")
                        break;
                }
                time.Sleep(time.Duration(3) * time.Second)
        }
        if pingErr != nil {
                log.Fatal("This is the error:", pingErr)
        }
        return pingErr
}

func (s *PacketCmdHndlrService) SendPacket(packet message.UpPacket) (error) {

	var err error
	if s.Enabled {
		fmt.Printf("PacketCmdHndlrService: Sending packet to %s\n", s.URL())
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(packet)
		err = Post(s.URL(), b)
	} else {
		fmt.Printf("PacketCmdHndlrService is disabled. Dropping packet.\n")
	}
	return err 

}
