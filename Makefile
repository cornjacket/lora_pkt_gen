mod_replace:
	go mod edit -replace bitbucket.org/cornjacket/lora_pkt_gen/services=/home/david/work/go/src/bitbucket.org/cornjacket/lora_pkt_gen/services
mod_drop_replace:
	go mod edit -dropreplace bitbucket.org/cornjacket/lora_pkt_gen/services
docker_local_bash:
	docker exec -it lora_pkt_gen /bin/ash
docker_run_prod:
	docker run -it --detach --name=lora_pkt_gen -p 8079:8079 cornjacket/lora_pkt_gen
docker_run_dev:
	docker run -it --name=lora_pkt_gen -p 8079:8079 lora_pkt_gen_dev 
docker_build_prod:
	docker build . -f ./docker/Dockerfile.prod -t cornjacket/lora_pkt_gen
docker_build_dev:
	docker build . -f ./docker/Dockerfile.dev -t lora_pkt_gen_dev
#aws_tag:
#	docker tag cornjacket/iot_app_4_cmd_hndlr 923799911166.dkr.ecr.us-west-1.amazonaws.com/iot_app_4_cmd_hndlr
#aws_login:
#	aws ecr get-login-password --region us-west-1 | docker login --username AWS --password-stdin 923799911166.dkr.ecr.us-west-1.amazonaws.com
#aws_push:
#	docker push 923799911166.dkr.ecr.us-west-1.amazonaws.com/iot_app_4_cmd_hndlr
consul_start_local:
	consul agent -dev -bind=127.0.0.1 -node=localhost
