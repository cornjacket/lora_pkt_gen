package main

import (
	"bitbucket.org/cornjacket/lora_pkt_gen/app"
)

func main() {

	pktApp := app.NewAppService()	
	pktApp.Init()
	pktApp.Run()

}

