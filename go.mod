module bitbucket.org/cornjacket/lora_pkt_gen

go 1.13

require (
	bitbucket.org/cornjacket/airtrafficcontrol v0.1.9
	bitbucket.org/cornjacket/cmd_hndlr v0.1.13
	bitbucket.org/cornjacket/discovery v0.1.0
	bitbucket.org/cornjacket/event_hndlr v0.1.13
	bitbucket.org/cornjacket/iot v0.1.9
	github.com/go-openapi/runtime v0.19.24
	github.com/gorilla/context v1.1.1
	github.com/julienschmidt/httprouter v1.3.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c
)
